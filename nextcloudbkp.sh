    #!/bin/sh
    # Copia de seguridad de nextcloud
    # Activar el modo mantenimiento
    sudo -u www-data php /var/www/html/nextcloud/occ maintenance:mode --on
    # Copiar la base de datos
    mysqldump --single-transaction -h localhost -u root -p'mi_contraseña' nextcloud > /media/usb/backups/nextcloud/database/nextcloud-sqlbkp_`date +"%Y%m%d"`.bak
    # Copia de seguridad de los archivos de configuración
    tar -cpzf /media/usb/backups/nextcloud/ncserver/ncserver-databkp_`date +"%Y%m%d"`.tar.gz /var/www/html/nextcloud
    # Copia de seguridad de los datos de Nextcloud
    tar -cpzf /media/usb/backups/nextcloud/data/nextcloud-databkp_`date +"%Y%m%d"`.tar.gz /media/nextcloud/data
    # Desactivar el modo mantenimiento
    sudo -u www-data php /var/www/html/nextcloud/occ maintenance:mode --off
    # Borrar las copias de seguridad más antiguas de 3 días
    sudo find /media/usb/backups/nextcloud/database -mtime +3 -exec rm {} \;
    sudo find /media/usb/backups/nextcloud/ncserver -mtime +3 -exec rm {} \;
    sudo find /media/usb/backups/nextcloud/data -mtime +3 -exec rm {} \;
